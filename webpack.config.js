const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        widget: './src/index.js',
    },
    output: {
        filename: '[name].min.js',
        path: path.resolve(__dirname, 'dist'),
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        host: "127.0.0.1",
        disableHostCheck: true,
        public: "http://127.0.0.1:3010",
        port: 3010,
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'example.html',
            template: 'public/example.html'
        }),
        new HtmlWebpackPlugin({
            filename: 'example2.html',
            template: 'public/example2.html'
        }),
	new HtmlWebpackPlugin({
            filename: 'example3.html',
            template: 'public/example3.html'
        }),
    ]
};
