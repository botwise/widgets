# Widgets BOTWISE

Widgets is a Javascript library enabling quick access to all your company knowledge 📖 using BOTWISE.  

Let your clients and employees quickly find the information the need directly from your website.

## Use cases

List of components to choose from:

* **chat** [[demo](https://static.botwise.io/scripts/example.html)]
* **livechat** [[demo](https://static.botwise.io/scripts/example2.html)]

## Installation

Insert the following line in between `<head></head>` tags of your page:
```html
<script src="https://static.botwise.io/scripts/widget.min.js"></script>
```

Next step is to use `BTWInit` function to configure the chosen component.

#### Chat

```html
<script>
  BTWInit('chat', {
      elementSelector: '#mountHere',
      clientName: 'test',
      initialQuery: 'question'
    })
</script>
```

The example above mounts the iframe inside an element with `mountHere` id and it opens the view with the first question from `initialQuery` property already asked.
Unique name of the tenant/instance should be always set via `clientName` property.
Given the arguments the functions generates a proper url for the iframe.

#### Livechat

```html
<script>
  BTWInit('liveChat', {
      elementSelector: '#mountHere',
      clientName: 'test',
      themeName: 'default'
    })
</script>
```

## Theme

The theme can be changes via `themeName` property choosing from the list predefined by BOTWISE. You can also [ask for a dedicated theme](mailto:contact@botwise.io).

Theme can be also changed via the `customization` parameter in config.
Available parameters:
```
/**
 * @typedef {Object} Customization
 * @property {string} userMessageBgColor example: "#0298d0"
 * @property {string} userMessageColor example: "#fff"
 * @property {string} messageColor example: "#383838"
 * @property {string} messageBgColor example: "#eaf1f7"
 * @property {string} messageOptionBgColor example: "#dae9f7"
 * @property {string} messageOptionFeaturedBgColor example: "to bottom, #0057b7 0% 50%, #ffd700 50% 100%" or just color #0057b7
 * @property {string} messageOptionFeaturedColorShadow example: "0px 0px 8px #0057b7"
 * @property {string} messageOptionFeaturedColor example: "#fff"
 * @property {string} messageOptionColor example: "#0298d0"
 * @property {string} messageLoaderColor example: "#0298d0"
 * @property {string} reportViewHeaderColor example: "#00376D"
 * @property {string} reportConfirmButtonBgColor example: "#00376D"
 * @property {string} reportCancelButtonColor example: "#00376D"
 * @property {string} avatarLocation example: "url(http://link-to.your/icon.png)"
 * @property {string} sendMessageButtonIconColor example: "#0298d0"
 * @property {string} sendMessageButtonBgColor example: "#0298d0"
 * @property {string} rootBgColor example: "#fff"
 * @property {string} chatBgColor example: "#fff"
 * @property {string} sendMessageInputBgColor example: "#fff"
 * @property {string} sendMessageInputColor example: "#fff"
 * @property {string} sendMessageButtonBgColor example: "#fff"
 * @property {string} sendMessageButtonColor example: "#fff"
 * @property {boolean | number} useOnlyOptions when set to true or 1 hides message input and allows user only to use messageOption buttons
 */
```

Livechat has extended config by two parameters:
```
/**
* @typedef {Object} LivechatColors
* @property {string} livechatBubbleBgColor background-color for livechat opening bubble
* @property {string} livechatBubbleLogoColor color for open livechat bubble icon
* @property {string} livechatBubbleCloseColor color for livechat close bubble icon
*/
```

The styling of the iframe can be also adjusted with `frameCss` property:
```js
BTWInit(
    'chat',
    {
        elementSelector: '#chat',
        initialQuery: 'chatbot',
        clientName: 'widgetdemo'
        frameCss: 'width: 300px; height: 200px;',
    }
)
```


## Development

Useful commands when developing the library:

`yarn` - installs all dependencies  
`yarn dev` - runs the development server at [locahost:3010](http://locahost:3010)  
`yarn build` - builds the production version

## Contact

Find out more at [BOTWISE.io](https://www.botwise.io/)  
Any questions, problems or suggestions? [Reach out to us directly](mailto:contact@botwise.io).
