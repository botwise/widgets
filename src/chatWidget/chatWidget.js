import { encodeStylesToUrlParameters } from "../helpers";

const addStyle = content => {
    const style = document.createElement('style');
    style.type = 'text/css';
    style.appendChild(document.createTextNode(content));
    document.head.appendChild(style);
}

const getFrameCode = src => (`
    <iframe src="${src}" frameborder="0" class="${uniqiFrameClass}">
        Iframes not working.
    </iframe>
`);

const sanitizeQuery = query => encodeURIComponent(query);

const uniqiFrameClass = `BTWIframe_${Math.random().toString(36).substring(7)}`;

const getIframeStyle = (customCss) => (`
    iframe.${uniqiFrameClass} {
        ${customCss}
        border: none;
    }
`);


export default (
    /**
     * @type {import("../types").WidgetConfig}
     */
{
    elementSelector,
    initialQuery = '',
    clientName,
    frameCss = '',
    themeName = '',
    customization = {}
}) => {
    const widgetPlacement = document.querySelectorAll(elementSelector)[0];
    if (!widgetPlacement) {
        console.error('BTW: element not found.', elementSelector);
        return;
    }
    if (!clientName) {
        console.error('BTW: clientName not set.');
        return;
    }
    frameCss && addStyle(getIframeStyle(frameCss));

    const stylesCustomization = encodeStylesToUrlParameters(customization);

    const isProduction = process.env.NODE_ENV === "production";
    widgetPlacement.innerHTML = getFrameCode(
        isProduction
          ? `https://${clientName}.botwise.io/embed/chatbot?query=${sanitizeQuery(initialQuery)}&theme=${themeName || clientName}${stylesCustomization ? "&" + stylesCustomization : ""}`
          : `http://localhost:3000/?query=${sanitizeQuery(initialQuery)}&theme=${themeName || clientName}${stylesCustomization ? "&" + stylesCustomization : ""}`
    )
}
