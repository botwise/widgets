/**
 * @typedef {Object} Customization
 * @property {string} userMessageBgColor example: "#0298d0"
 * @property {string} userMessageColor example: "#fff"
 * @property {string} messageColor example: "#383838"
 * @property {string} messageBgColor example: "#eaf1f7"
 * @property {string} messageOptionBgColor example: "#dae9f7"
 * @property {string} messageOptionFeaturedBgColor example: "to bottom, #0057b7 0% 50%, #ffd700 50% 100%" or just color #0057b7
 * @property {string} messageOptionFeaturedColorShadow example: "0px 0px 8px #0057b7"
 * @property {string} messageOptionFeaturedColor example: "#fff"
 * @property {string} messageOptionColor example: "#0298d0"
 * @property {string} messageLoaderColor example: "#0298d0"
 * @property {string} reportViewHeaderColor example: "#00376D"
 * @property {string} reportConfirmButtonBgColor example: "#00376D"
 * @property {string} reportCancelButtonColor example: "#00376D"
 * @property {string} avatarLocation example: "url(http://link-to.your/icon.png)"
 * @property {string} sendMessageButtonIconColor example: "#0298d0"
 * @property {string} sendMessageButtonBgColor example: "#0298d0"
 * @property {string} rootBgColor example: "#fff"
 * @property {string} chatBgColor example: "#fff"
 * @property {string} sendMessageInputBgColor example: "#fff"
 * @property {string} sendMessageInputColor example: "#fff"
 * @property {string} sendMessageButtonBgColor example: "#fff"
 * @property {string} sendMessageButtonColor example: "#fff"
 * @property {boolean | number} useOnlyOptions when set to true or 1 hides message input and allows user only to use messageOption buttons
 */

/**
 * @typedef {Object} WidgetConfig
 * @property {string} elementSelector selector to pickup element for mounting widget by querySelector, example: "#chat"
 * @property {string} initialQuery a query to be send by default, example: "chatbot"
 * @property {string} clientName clientName for right instance to be mount, example: "widgetdemo"
 * @property {string} frameCss custom styles for iframe, example: "width: 300px; height: 200px;"
 * @property {string} themeName theme name added for you by Botwise Widget creators
 * @property {Customization} customization an object which allows you to customize chatbot elements (colors, icons, backgrounds)
 */

/**
 * @typedef {Object} LiveChatConfig
 * @property {string} elementSelector selector to pickup element for mounting widget by querySelector, example: "#chat"
 * @property {string} initialQuery a query to be send by default, example: "chatbot"
 * @property {string} clientName clientName for right instance to be mount, example: "widgetdemo"
 * @property {string} frameCss custom styles for iframe, example: "width: 300px; height: 200px;"
 * @property {string} themeName theme name added for you by Botwise Widget creators
 * @property {Object} tags tags
 * @property {Customization & { livechatBubbleBgColor: string, livechatBubbleLogoColor: string, livechatBubbleCloseColor: string }} customization an object which allows you to customize chatbot elements (colors, icons, backgrounds)
 */

export default {};