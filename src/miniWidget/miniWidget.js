export default ({ selector, destination }) => {
    const widgetPlacement = document.querySelectorAll(selector)[0];
    if (widgetPlacement) {
        widgetPlacement.innerHTML = `
            <div>test</div>
        `;
    } else {
        console.error('BTW: element not found.', selector)
    }
}