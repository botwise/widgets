/**
 * 
 * @param {Object} styles 
 * @returns {string | undefined}
 */

export const encodeStylesToUrlParameters = (styles) => {
    if (!styles || typeof styles !== "object" || !Object.keys(styles).length) return undefined;

    return Object.keys(styles).map(key => {
        if (styles[key]) {
            return `${key}=${encodeURIComponent(styles[key])}`;
        }
        return undefined;
    }).filter(item => item !== undefined).join("&");
}
