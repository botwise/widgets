import { encodeStylesToUrlParameters } from "../helpers";

const addStyle = content => {
    const style = document.createElement('style');
    style.type = 'text/css';
    style.appendChild(document.createTextNode(content));
    document.head.appendChild(style);
}

const getFrameCode = (src) => {
    const frame = document.createElement('iframe')
    frame.src = src
    frame.id = 'widgetFrame'
    frame.scrolling = 'no'
    frame.className = uniqiFrameClass
    return frame;
};

const sanitizeQuery = query => encodeURIComponent(query);

const uniqiFrameClass = `BTWIframe_${Math.random().toString(36).substring(7)}`;

const getIframeStyle = (customCss) => (`
    iframe.${uniqiFrameClass} {
        ${customCss}
    }
`);


export default (
    /**
     * @type {import("../types").LiveChatConfig}
     */
    {
    elementSelector,
    initialQuery = '',
    clientName,
    frameCss = '',
    themeName = '',
    tags = {},
    customization = {}
}) => {
    const changeStyle = (x, clientName) => {
        const frame = document.getElementById('widgetFrame');
        const frameStyle = `
            border: none;
            bottom: 0;
            right: 0;
            position: fixed;
            overflow: hidden;
        `
        if (x.matches && frame) {
            frame.style = `
                ${frameStyle}
                width: 100%;
                height: 100%;
            `
        } else {
            frame.style = `
                ${frameStyle}
                width: 40%;
                height: 80vh;
            `;
        }
    }
    // wait for document to load
    document.addEventListener("DOMContentLoaded", function (event) {
        // watch width changes
        const width = window.matchMedia("(max-width: 700px)")
        // add custom style
        changeStyle(width);
        width.addListener(changeStyle);
    });
    const widgetPlacement = document.querySelectorAll(elementSelector)[0];

    if (!widgetPlacement) {
        console.error('BTW: element not found.', elementSelector);
        return;
    }
    if (!clientName) {
        console.error('BTW: clientName not set.');
        return;
    }

    const stylesCustomization = encodeStylesToUrlParameters(customization);

    frameCss && addStyle(getIframeStyle(frameCss));
    const isProduction = process.env.NODE_ENV === "production";
    const tagsUrlEncoded = encodeURIComponent(JSON.stringify(tags));
    const frameParams = `query=${sanitizeQuery(initialQuery)}&theme=${themeName || clientName}&tags=${tagsUrlEncoded}${stylesCustomization ? "&" + stylesCustomization : ""}`;
    const targetOrigin = isProduction ? `https://${clientName}.botwise.io` : `http://localhost:3090`
    const iframe = getFrameCode(`${targetOrigin}/embed/livechat?${frameParams}`);
    widgetPlacement.appendChild(iframe);
}
