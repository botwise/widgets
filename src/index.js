import chat from './chatWidget/chatWidget';
import liveChat from './liveWidget/liveWidget';
// import widget from './miniWidget/miniWidget';

const functionsMap = {
    // widget,
    chat,
    liveChat
}

/**
 * 
 * @param {"chat" | "liveChat"} type 
 * @param {import("./types").WidgetConfig | import("./types").LiveChatConfig} config 
 */
window.BTWInit = (type, config) => {
    functionsMap[type](config);
}