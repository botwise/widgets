# Widgets BOTWISE

Widgets jest biblioteką Javascriptową umożliwiającą szybki dostęp do informacji z wykorzystaniem BOTWISE.

Pozwala na osadzenie na wybranej stronie komponentów zapewniających wygodny dostęp do wiedzy zbieranej na platformie.

## Zastosowania

Dostępne są następujące komponenty/widgety:

* **chat** [[demo](https://static.botwise.io/scripts/example.html)]
* **livechat** [[demo](https://static.botwise.io/scripts/example2.html)]

## Instalacja

W celu użycia biblioteki należy między tagami `<head></head>` załączyć dodatkowo następujący skrypt:
```html
<script src="https://static.botwise.io/scripts/widget.min.js"></script>
```

Nąstępnie użyć funkcji `BTWInit` konfigurującej wybrany komponent.

#### Chat

```html
<script>
  BTWInit('chat', {
      elementSelector: '#mountHere',
      clientName: 'test',
      initialQuery: 'question'
    })
</script>
```

Powyższy przykład zamontuje iframe w elemencie z id `mountHere` i przekaże do niego `question` z parametru `initialQuery`.
Nazwę instancji klienta należy podać w parametrze `clientName`. Na podstawie wprowadzonych ustawień generowany jest dokładny adres url iframe'a.

#### Livechat

```html
<script>
  BTWInit('liveChat', {
      elementSelector: '#mountHere',
      clientName: 'test',
      themeName: 'default'
    })
</script>
```

## Wygląd

Zastosowany w komponencie motyw można zmienić poprzez ustawienie wartości `themeName` z listy predefiniowanej przez BOTWISE.

Wygląd może zostać zmieniony także za pomocą parametru `customization`.
Obsługiwane parametry:
```
/**
 * @typedef {Object} Customization
 * @property {string} userMessageBgColor przykład: "#0298d0"
 * @property {string} userMessageColor przykład: "#fff"
 * @property {string} messageColor przykład: "#383838"
 * @property {string} messageBgColor przykład: "#eaf1f7"
 * @property {string} messageOptionBgColor przykład: "#dae9f7"
 * @property {string} messageOptionFeaturedBgColor przykład: "to bottom, #0057b7 0% 50%, #ffd700 50% 100%" lub po prostu kolor #0057b7
 * @property {string} messageOptionFeaturedColorShadow przykład: "0px 0px 8px #0057b7"
 * @property {string} messageOptionFeaturedColor przykład: "#fff"
 * @property {string} messageOptionColor przykład: "#0298d0"
 * @property {string} messageLoaderColor przykład: "#0298d0"
 * @property {string} reportViewHeaderColor przykład: "#00376D"
 * @property {string} reportConfirmButtonBgColor przykład: "#00376D"
 * @property {string} reportCancelButtonColor przykład: "#00376D"
 * @property {string} avatarLocation przykład: "url(http://link-to.your/icon.png)"
 * @property {string} sendMessageButtonIconColor example: "#0298d0"
 * @property {string} sendMessageButtonBgColor przykład: "#0298d0"
 * @property {string} rootBgColor przykład: "#fff"
 * @property {string} chatBgColor przykład: "#fff"
 * @property {string} sendMessageInputBgColor przykład: "#fff"
 * @property {string} sendMessageInputColor przykład: "#fff"
 * @property {string} sendMessageButtonBgColor przykład: "#fff"
 * @property {string} sendMessageButtonColor przykład: "#fff"
 * @property {boolean | number} useOnlyOptions kiedy ustawione na true lub 1, chowa pole wiadomości i pozwala użytkownikom na korzystanie tylko z opcji w wiadomościach
 */
```

Livechat ma rozszerzoną konfigurację o dwa dodatkowe parametry:
```
/**
* @typedef {Object} LivechatColors
* @property {string} livechatBubbleBgColor kolor tła dla ikonki otwierającej chat
* @property {string} livechatBubbleLogoColor kolor ikony logo otwierającej livechat
* @property {string} livechatBubbleCloseColor kolor ikony zamykającej livechat
*/
```

Style wygenerowanego `<iframe>` możemy dodatkowo dostosować. Dodawana jest automatycznie generowana nazwa klasy (np. `BTWIframe_j5jdudm`) a tag `<style></style>` jest dodawany do `head`. Style mogą być globalne (na poziomie skryptu) lub przekazane w wywołaniu w parametrze `frameCss`:

```js
BTWInit(
    'chat',
    {
        elementSelector: '#chat',
        initialQuery: 'chatbot',
        clientName: 'widgetdemo'
        frameCss: 'width: 300px; height: 200px;',
    }
)
```

## Development

Komendy wykorzystywane przy developmencie biblioteki:

`yarn` - instalacja zależności  
`yarn dev` - uruchomienie serwera developerskiego pod adresem [locahost:3010](http://locahost:3010)  
`yarn build` - zbudowanie wersji produkcyjnej

## Kontakt

Więcej informacji na [BOTWISE.io](https://www.botwise.io/)  
W przypadku pytań, problemów bądź sugestii zapraszamy do [kontaktu mailowego](mailto:contact@botwise.io).
